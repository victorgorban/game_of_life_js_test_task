'use strict'

// вычисление всевдорандома, без библиотек. По производительности чуть хуже Math.random, но Math.sin дает более интересные и юзабельные для игры "жизнь" результаты.
// Основная причина использования sin - он всегда дает одинаковый результат для указанного seed.
function random(seed) {
    let x = Math.sin(seed) * 10000;
    return x - Math.floor(x);
}

class LifeField {
    static WIDTH = 2000;
    static HEIGHT = 2000;

    constructor() {
        this.field = new ArrayBuffer(LifeField.WIDTH + LifeField.WIDTH * LifeField.HEIGHT / 2);
        this.temp = new ArrayBuffer(LifeField.WIDTH + LifeField.WIDTH * LifeField.HEIGHT / 2);
        // Клетки поля. Одномерный массив а не многомерный, потому что так производительнее.
        // Работать с fieldInts в плане битовых операций конечно быстрее, но fieldBytes проецируется на canvas (1к1), поэтому пришлось оставить также fieldBytes.
        this.fieldBytes = new Uint8Array(this.field);
        this.fieldInts = new Uint32Array(this.field);
        // Временные данные. tempBytes - это из прошлой версии алгоритма. Она была более понятной, но менее производительной. Битовые операции на uint32 оказались намного быстрее чем простое сложение и сравнение байтов.
        // this.tempBytes = new Uint8Array(this.temp);
        this.tempInts = new Uint32Array(this.temp);
        // Номер поголения
        this.generation = 0;
    }

    /**
     * Получить пиксель(клетку) по указанным координатам. Учитывает границы поля, также учитывает то что в одном байте хранится информация о двух клетках.
     * @param {*} x 
     * @param {*} y 
     * @returns 
     */
    get(x, y) {
        const pos = LifeField.WIDTH / 2 + y * (LifeField.WIDTH / 2) + (x >> 1);
        if ((x & 1) == 1) return (this.fieldBytes[pos] & 0x10) == 0x10;
        else return (this.fieldBytes[pos] & 1) == 1;
    }

    /**
     * Задать состояние пикселю(клетке) по указанным координатам.
     * @param {*} x 
     * @param {*} y 
     * @returns 
     */
    set(x, y, val) {
        const pos = LifeField.WIDTH / 2 + y * (LifeField.WIDTH / 2) + (x >> 1);
        if ((x & 1) == 1) {
            if (val) this.fieldBytes[pos] |= 0x10;
            else this.fieldBytes[pos] &= (0xFF & ~0x10);
        }
        else {
            if (val) this.fieldBytes[pos] |= 0x1;
            else this.fieldBytes[pos] &= (0xFF & ~0x1);
        }
    }

    /**
     * Очищает поле
     */
    clear() {
        this.generation = 0;
        // Очистка поля.
        // TODO можно просто создать пустой массив, или использовать Array.fill. Нужно сравнить производительность
        for (let x = 0; x < LifeField.WIDTH; x++) {
            for (let y = 0; y < LifeField.HEIGHT; y++) {
                this.set(x, y, false);
            }
        }
    }

    /**
     * Получает кол-во живых клеток на поле. Ничего быстрее простейшего прохода по массиву еще не придумали, поэтому здесь юзается простейший проход по массиву, вместо Array.reduce и подобных.
     */
    count() {
        let cnt = 0;
        for (let x = 0; x < LifeField.WIDTH; x++) {
            for (let y = 0; y < LifeField.HEIGHT; y++) {
                if (this.get(x, y)) cnt++;
            }
        }
        return cnt;
    }

    /**
     * Инициализирует поле на основе рандомного сида.
     * @param {*} seed - семя типа 12345
     * @param {*} aliveProbability - вероятность появления живой клетки
     */
    initializeWithRandom(seed, aliveProbability) {
        this.generation = 0;
        for (let x = 1; x < LifeField.WIDTH - 1; x++) {
            for (let y = 1; y < LifeField.HEIGHT - 1; y++) {
                const val = random(seed++);
                this.set(x, y, val < aliveProbability);
            }
        }
    }

    /**
     * Инициализирует поле паттерном из файла. Выбирает соотв. функцию в зависимости от паттерна.
     * @param {String} contents текстовые данные файла
     */
    initializeWithFile(contents) {
        contents = contents.trim();
        if (contents[contents.length - 1] == '!')
            this.initializeWithLif(contents);
        else
            this.initializeWithFileLife15(contents);
    }

    /**
     * Инициализирует поле паттерном из файла. Паттерн в формате life_15
     * @param {String} contents текстовые данные файла
     */
    initializeWithFileLife15(contents) {
        this.clear();
        const lines = contents.split("\n");
        let shiftX = 0;
        let shiftY = 0;
        let y = LifeField.HEIGHT / 2 + shiftX;
        let x = LifeField.WIDTH / 2 + shiftY;
        let index = 0;
        while (index < lines.length) {
            const line = lines[index++].trim();
            if (line.length == 0) continue;
            else if (line[0] == '#') {
                if (line[1] == 'P') {
                    x = LifeField.WIDTH / 2 + shiftX + parseInt(line.split(' ')[1]);
                    y = LifeField.HEIGHT / 2 + shiftY + parseInt(line.split(' ')[2]);
                }
            }
            else if (line[0] == '.' || line[0] == '*') {
                for (let j = 0; j < line.length; j++) {
                    this.set(x + j, y, line[j] == '*');
                }
                y++;
            }
        }
    }

    /**
     * Инициализирует поле паттерном из файла. Паттерн в формате LIF
     * @param {String} contents текстовые данные файла
     */
    initializeWithLif(contents) {
        this.clear();
        const lines = contents.split("\n");
        let shiftX = 0;
        let shiftY = 0;
        let y = LifeField.HEIGHT / 2 + shiftX;
        let x = LifeField.WIDTH / 2 + shiftY;
        let index = 0;
        let index_x = x;
        let num = 0;
        while (index < lines.length) {
            const line = lines[index++].trim();
            if (line.length == 0) continue;
            else if (line[0] == '#') {
                if (line[1] == 'P') {
                    x = LifeField.WIDTH / 2 + shiftX + parseInt(line.split(' ')[1]);
                    y = LifeField.HEIGHT / 2 + shiftY + parseInt(line.split(' ')[2]);
                }
            }
            else if (line[0] == 'x') {
                x -= parseInt(line.split(' ')[2]) >> 1;
                y -= parseInt(line.split(' ')[5]) >> 1;
                index_x = x;
            }
            else {
                let pos = 0;
                while (pos < line.length) {
                    if (line[pos] == '$') {
                        if (num == 0) num = 1;
                        y += num;
                        index_x = x;
                        num = 0;
                    }
                    else if (line[pos] == 'b') {
                        if (num == 0) num = 1;
                        index_x += num;
                        num = 0;
                    }
                    else if (line[pos] == 'o') {
                        if (num == 0) num = 1;
                        for (let i = 0; i < num; i++) {
                            this.set(index_x++, y, true);
                        }
                        num = 0;
                    }
                    else if (line[pos] >= '0' && line[pos] <= '9') {
                        num = num * 10 + (line[pos] - '0');
                    }
                    pos++;
                }
            }

        }
    }

    /**
     * Осуществляет шаг. Производительность постоянная, т.к. фактически просто проходит по всему массиву и проверяет соседей у каждой клетки. 
     * В соответствии с логикой игры, клетка оживает, умирает или не изменяется. 
     * Активно используется битовая логика, код тяжел для понимания. Я максимально подробно все описал. Если все еще непонятно - нужно подучить булевую алгебру.
     * Фактически это оптимизированный в битовом понимании код для подсчета соседей у клетки. Такая себе матрица 3x3, где в каждой клетке может быть 0 или 1. 
     * 8 соседей плюс 1 рассматриваемая клетка.
     */
    makeStep() {
        this.generation++;

        // заполнение временного массива-поля нулями.
        // array.fill(0) в разы медленнее обычного заполнения через цикл.
        for (let i = 0; i < this.tempInts.length; i++) this.tempInts[i] = 0;

        // В этом цикле мы заполняем данные о соседях клетки. Данные о соседях используются в следующем цикле.
        // здесь мы заполняем данные о соседях для каждой клетки. Заносим их в массив tempInts
        // почему тут шаги по 4: потому что тут мы работаем с int32 вместо байтов. int32 больше byte в 4 раза. 
        // За счет снижения кол-ва итераций в массиве (битовые операции намного быстрее обычных) в несколько раз выросла производительность шага (в сравнении с использованием прохода по массиву байтов).
        // элементарно, присвоить значение int32 в разы быстрее, чем 4 раза присвоить значение int8.
        for (let j = LifeField.WIDTH; j < LifeField.WIDTH * LifeField.HEIGHT / 2; j += 4) {
            const i = j / 4;

            // логика названий: 0 это i, _1 это i-1, 1 это i+1. Вторая цифра: 0 это i, _1 это i-LifeField.WIDTH, 1 это i+LifeField.WIDTH
            // Почему 8: потому что 4*2. В каждом 32-битном числе находится по 8 ячеек данных-клеток, всего 32 бита. По 4 бита на каждую клетку, нужно для битовых операций.
            // 8 соседей, значения которые потом складываются. Плюс данные о состоянии целевой ячейки (src00).
            // i-width/8, i+width/8 и так далее - так сделано потому, что массив одномерный. Двумерный массив понятнее, но он и медленнее будет в данном случае.
            const src01 = this.fieldInts[i - LifeField.WIDTH / 8];
            const src00 = this.fieldInts[i];
            const src02 = this.fieldInts[i + LifeField.WIDTH / 8];

            // -1,+1 - это соседи короче, с той или с другой стороны.
            const src_11 = this.fieldInts[i - LifeField.WIDTH / 8 - 1];
            const src_10 = this.fieldInts[i - 1];
            const src_12 = this.fieldInts[i + LifeField.WIDTH / 8 - 1];

            const src11 = this.fieldInts[i - LifeField.WIDTH / 8 + 1];
            const src10 = this.fieldInts[i + 1];
            const src12 = this.fieldInts[i + LifeField.WIDTH / 8 + 1];

            // булевая алгебра становится понятнее, если увидеть как представляется число в битах.
            // << и >> это битовые сдвиги, >> 4 значит "сдвинуть байты на 4 бита вправо". << 28 значит "сдвинуть биты на 28 бит влево". 
            // При сдвиге "освобожденные" биты "отбрасываются", то есть приобретают значение, аналогичное 0.
            // ~a - заменяет каждый бит числа на противоположный (0 <> 1)
            // в итоге в битовом представлении отражается наличие соседей.
            this.tempInts[i] += (src01 << 4) + src01 + (src01 >> 4);
            this.tempInts[i] += (src00 << 4) + (src00 >> 4);
            this.tempInts[i] += (src02 << 4) + src02 + (src02 >> 4);

            this.tempInts[i] += (src_11 >> 28) + (src_10 >> 28) + (src_12 >> 28);
            this.tempInts[i] += (src11 << 28) + (src10 << 28) + (src12 << 28);
        }

        // В этом цикле обрабатываются данные о соседях (живые они или мертвые), и на основании количества мы решаем, что делать с обрабатываемой клеткой.
        // здесь происходит битовое сложение
        for (let j = LifeField.WIDTH; j < LifeField.WIDTH * LifeField.HEIGHT / 2; j += 4) {
            const i = j / 4;
            // в this.tempInts[i] находится кол-во соседей.
            const neighours = this.tempInts[i] & 0x77777777; // 1110111011101110111011101110111, 111
            const cellToProcess = this.fieldInts[i];

            let keepAlive = ((neighours & ~0x11111111) >> 1) | (cellToProcess << 2);
            keepAlive ^= ~0x55555555; // 1010101010101010101010101010101, 101
            keepAlive &= (keepAlive >> 2);
            keepAlive &= (keepAlive >> 1);
            keepAlive &= 0x11111111; // 10001000100010001000100010001, 1

            let makeNewLife = neighours | (cellToProcess << 3);
            makeNewLife ^= ~0x33333333; // 110011001100110011001100110011, 11
            makeNewLife &= (makeNewLife >> 2);
            makeNewLife &= (makeNewLife >> 1);
            makeNewLife &= 0x11111111; // 10001000100010001000100010001, 1

            this.fieldInts[i] = keepAlive | makeNewLife;
        }

        for (let y = 1; y < LifeField.WIDTH - 1; y++) {
            this.set(0, y, false);
            this.set(LifeField.WIDTH - 1, y, false);
        }
    }

    /**
     * Пройти [steps] шагов. 
     * @param {*} steps количество шагов, которое нужно сделать. 
     * @returns {int} - время исполнения шага
     */
    run(steps) {
        const startMillis = performance.now();
        for (let i = 0; i < steps; i++) this.makeStep();
        const endMillis = performance.now();
        return endMillis - startMillis;
    }

    /**
     * Виртуальная "отрисовка" в массиве. Заполнение данными.
     * @param {Array} imagePixels 
     */
    draw(imagePixels) {
        for (let i = 0; i < imagePixels.length; i += 2) {
            const val = this.fieldBytes[LifeField.WIDTH / 2 + (i >> 1)];
            imagePixels[i] = (val & 1) == 0 ? 0xFF000000 : 0xFFFFFFFF;
            imagePixels[i + 1] = (val & 16) == 0 ? 0xFF000000 : 0xFFFFFFFF;
        }
    }

}

// LifeField.WIDTH = 1920;
// LifeField.HEIGHT = 1080;

