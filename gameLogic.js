'use strict'

/** инстанс LifeField, содержит всю инфу об актуальном состоянии поля. */
let lifeField;

let canvasContext;
// canvasContext.createImageData
let imageData;
/** Данные о пикселях изображения. Фактически белый или черный цвет. */
let imagePixels;
/** Вспомогательный массив-буфер для заполнения данных пикселей изображения. */
let imageClampedArray;

/** htmlElement, который содержит данные о производительности итераций за секунду. Кол-во итераций в секунду, может еще что-то добавлю. */
let performanceHtml;

/** время последней итерации */
let lastElapsed = performance.now();
/** всего шагов/итераций до обновления статуса. updateStatus обнуляет это значение. */
let countedSteps = 0;
/** количество шагов на кадр. от 1 до 20. Выставляется в интерфейсе */
let stepsPerFrame = 1;
let isStepsManual = false;

let isGameRunning = false;
/** true если любой из модалок отображен. */
let isDialogVisible = false;

/** 'random' || 'patternFile' */
let loadedPatternType = 'random';

/**
 * Инициализирует поле. Создает начальный массив пикселей, 
 */
function start() {
    performanceHtml = document.getElementById("performance");

    lifeField = new LifeField();

    const canvas = document.getElementById("field");
    canvasContext = canvas.getContext('2d');
    imageData = canvasContext.createImageData(LifeField.WIDTH, LifeField.HEIGHT);
    let imageBuffer = new ArrayBuffer(imageData.data.length);
    imagePixels = new Uint32Array(imageBuffer);
    // если кто не знает: ClampedArray инициализируется нулями + обрезает значения вне байт-значений [0, 255] не обрезанием битов (0.9=>0), а математическим округлением (0.9=>1). Особенно это полезно для неточных значений.
    // для рисования битовой графики Uint8ClampedArray подходит намного лучше чем обычный Uint8Array.
    imageClampedArray = new Uint8ClampedArray(imageBuffer);

    window.addEventListener('keydown', keyPressed);

    drawCanvas();
    //loadRandom();
    showDialogById('dialog_randomseed');
}

function keyPressed(e) {
    if (isDialogVisible) return;
    if (e.keyCode == 32) {
        e.stopPropagation();
        e.preventDefault();
        if (isGameRunning) stopSimulation();
        else startSimulation();
    }
}

function makeStep() {
    if (isGameRunning) {
        countedSteps += stepsPerFrame;
        lifeField.run(stepsPerFrame);
    }

    drawCanvas();

    if (isGameRunning && !isStepsManual) {
        // requestAnimationFrame выполняет функцию до следующего обновления монитора или браузера. Позволяет максимально быстро отобразить результат, но с привязкой к кадру.
        window.requestAnimationFrame(makeStep);
    } else {
        return;
    }
}

/**
 * Рисует данные из массива на canvas.
 */
function drawCanvas() {
    lifeField.draw(imagePixels);
    imageData.data.set(imageClampedArray);
    // putImageData отображает данные из imageData на холсте. По логике похоже на рисование bitmap.
    canvasContext.putImageData(imageData, 0, 0);
    updatePerformanceStatusAndStopIfDead();
}


function updatePerformanceStatusAndStopIfDead() {
    const nowElapsed = performance.now();
    if (isStepsManual || !isGameRunning || nowElapsed - lastElapsed >= 1000) {
        const stepsPerSecond = countedSteps * 1000.0 / (nowElapsed - lastElapsed);

        let aliveCount = lifeField.count();

        let status =
            stepsPerSecond == 0 ?
                `Поколение: ${lifeField.generation}. Живых: ${aliveCount}` :
                `Поколение: ${lifeField.generation}. Шагов в секунду: ${Math.round(stepsPerSecond)}. Живых: ${aliveCount}`;
        performanceHtml.innerText = status;
        countedSteps = 0;
        lastElapsed = nowElapsed;

        if (stepsPerSecond != 0 && aliveCount == 0) {
            if (!isGameRunning) return;
            stopSimulation();
        }
    }
}

function startSimulation() {
    if (isStepsManual) {
        lastElapsed = performance.now();
        countedSteps = stepsPerFrame;
        lifeField.run(stepsPerFrame);
        drawCanvas();
        disableButtonById('pauseButton');
        enableButtonById('startSimulationButton');
        return;
    }

    if (isGameRunning) return;
    isGameRunning = true;
    lastElapsed = performance.now();
    countedSteps = 0;
    enableButtonById('pauseButton');
    disableButtonById('startSimulationButton');
    window.requestAnimationFrame(makeStep);
}

function stopSimulation() {
    disableButtonById('pauseButton');
    enableButtonById('startSimulationButton');
    isGameRunning = false;
    drawCanvas();
}


function initSimulationWithNewPattern() {
    stopSimulation();
    if (loadedPatternType == 'random') loadRandom();
    else loadTextPattern();
    drawCanvas();
}

function handleSelectSpeed() {
    const speedSelect = document.getElementById("speed");
    const selectedSpeed = speedSelect.options[speedSelect.selectedIndex].value;
    stepsPerFrame = parseInt(selectedSpeed);
    // отрицательное число задается при выставлении опций скорости мануальной.
    if (stepsPerFrame == 0) {
        stepsPerFrame = 0;
        isStepsManual = true;
        stopSimulation();
    }
    else {
        isStepsManual = false;
    }
}

/**
 * Загружает текстовый контент-паттерн
 */
function loadTextPattern() {
    stopSimulation();
    const lifText = document.getElementById("pattern_text").value;
    lifeField.initializeWithFile(lifText);
    loadedPatternType = 'patternFile';
    drawCanvas();
}

function loadRandom() {
    stopSimulation();
    const seed = document.getElementById("random_seed").value;
    const threshold = document.getElementById("random_cell_alive_probability").value;
    lifeField.initializeWithRandom(seed, threshold);
    loadedPatternType = 'random';
    drawCanvas();
}

function showDialogLif() {
    showDialogById('dialog_file_pattern');
    // focus нужен для того, чтобы при клавишной навигации нажатия не регистрировались на поле, например.
    document.getElementById('dialog_file_pattern').focus();
}

function showDialogById(id) {
    if (isDialogVisible) return;
    const element = document.getElementById(id);
    element.classList.remove('not-visible');
    isDialogVisible = true;
}

function hideDialogById(id) {
    const element = document.getElementById(id);
    element.classList.add('not-visible');
    isDialogVisible = false;
}

function disableButtonById(id) {
    const element = document.getElementById(id);
    element.setAttribute('disabled', '');
}

function enableButtonById(id) {
    // TODO тут нужно атрибут disabled добавлять, а не классы
    const element = document.getElementById(id);
    element.removeAttribute('disabled');
}
